package com.mhamdi.showroomz.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.mhamdi.showroomz.BuildConfig;
import com.mhamdi.showroomz.R;

public class ExteriorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exterior);
        WebView webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);


        ImageButton QuitPanoramaBtn = findViewById(R.id.close);
        QuitPanoramaBtn.setOnClickListener(view -> finish());

        Intent intent = getIntent();
        String imgUrl = intent.getStringExtra(DetailActivity.EXTERIOR_URL);
        if (BuildConfig.DEBUG)
            Log.d("url", "url: " + imgUrl);
        webView.loadUrl(imgUrl);
    }
}
