package com.mhamdi.showroomz.presentation.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mhamdi.showroomz.BuildConfig;
import com.mhamdi.showroomz.R;
import com.mhamdi.showroomz.data.models.Item;
import com.mhamdi.showroomz.presentation.DetailActivity;

import java.util.List;


public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> {

    public static final String MODEL_ID = "modelId";
    public static final String ITEM_IMAGE_TRANSITION = "item_image_transition";
    public static final String ITEM_PRICE_TRANSITION = "item_price_transition";
    private List<Item> itemList;
    private Context context;
    private Activity activity;

    public ItemAdapter(List<Item> itemList, Context context, Activity activity) {
        this.itemList = itemList;
        this.context = context;
        this.activity = activity;
    }


    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder, final int position) {

        holder.name.setText(itemList.get(position).getModelsNameEn());
        holder.price.setText(itemList.get(position).getModelsPrice());
        Glide.with(context).load(itemList.get(position).getModelsImage())
                .placeholder(R.drawable.vehicle_placeholder)
                .into(holder.image);
        if (BuildConfig.DEBUG)
            Log.d("TAG", "onBindViewHolder: " + itemList.get(position).getModelsImage());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(MODEL_ID, itemList.get(position).getModelsId());
           // ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, holder.image, ITEM_IMAGE_TRANSITION);

            Pair<View, String> p2 = Pair.create(holder.price, ITEM_PRICE_TRANSITION);
            Pair<View, String> p3 = Pair.create(holder.image, ITEM_IMAGE_TRANSITION);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, p2, p3);
            context.startActivity(intent, options.toBundle());
        });
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }


    static class ItemHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        TextView price;

        private ItemHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_name);
            price = itemView.findViewById(R.id.item_price);
            image = itemView.findViewById(R.id.item_detail_image);
        }

    }
}