package com.mhamdi.showroomz.presentation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.mhamdi.showroomz.BuildConfig;
import com.mhamdi.showroomz.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.mhamdi.showroomz.presentation.DetailActivity.INTERIOR_URL;

public class InteriorActivity extends AppCompatActivity {

    VrPanoramaView vrView;

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interior);
        ImageButton Quit = findViewById(R.id.close);
        Quit.setOnClickListener(view -> finish());
        vrView = findViewById(R.id.vr_view);
        vrView.setTouchTrackingEnabled(true);


        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra(INTERIOR_URL);
            if (BuildConfig.DEBUG)
                Log.d("intentUrl", url);

            new Thread(() -> loadPhotoSphere(url)).start();
        }

    }

    private void loadPhotoSphere(String url) {

        Bitmap bitmap = getBitmapFromURL("http://showroomz.com/" + url);
        if (!isFinishing()) {
            VrPanoramaView.Options options = new VrPanoramaView.Options();
            options.inputType = VrPanoramaView.Options.TYPE_MONO;
            vrView.loadImageFromBitmap(bitmap, options);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        vrView.pauseRendering();
    }

    @Override
    protected void onResume() {
        super.onResume();
        vrView.resumeRendering();
    }

    @Override
    protected void onDestroy() {
        vrView.shutdown();
        super.onDestroy();
    }
}
