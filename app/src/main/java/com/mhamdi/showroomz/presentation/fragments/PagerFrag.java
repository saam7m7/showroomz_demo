package com.mhamdi.showroomz.presentation.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.mhamdi.showroomz.R;
import com.mhamdi.showroomz.data.models.SpecModel;
import com.mhamdi.showroomz.presentation.adapter.CustomExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mhamdi.showroomz.presentation.adapter.specModelsVIewPagerAdapter.SPEC_MODEL;


public class PagerFrag extends Fragment {

    private SpecModel specModel;


    public PagerFrag() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);


        ExpandableListView expandableListView = view.findViewById(R.id.expandableListView);


        Bundle args = getArguments();
        if (args != null) {
            specModel = (SpecModel) getArguments().getSerializable(SPEC_MODEL);
            if (specModel != null) {

                HashMap<String, List<String>> expandableListDetail = getData();
                List<String> expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
                ExpandableListAdapter expandableListAdapter = new CustomExpandableListAdapter(getContext(), expandableListTitle, expandableListDetail);
                expandableListView.setAdapter(expandableListAdapter);
            }

        }

        return view;
    }

    public HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<>();


        List<String> driveTrainTitle = new ArrayList<>();
        List<String> interioTitle = new ArrayList<>();
        List<String> extriorTitle = new ArrayList<>();
        List<String> safetyTitle = new ArrayList<>();

        if (specModel.getDriveTrain() != null) {
            for (int i = 0; i < specModel.getDriveTrain().getProperties().size(); i++) {
                driveTrainTitle.add(specModel.getDriveTrain().getProperties().get(i).getValue());
            }
            expandableListDetail.put(specModel.getDriveTrain().getName(), driveTrainTitle);
        }

        if (specModel.getExterior() != null) {
            for (int i = 0; i < specModel.getExterior().getProperties().size(); i++) {
                driveTrainTitle.add(specModel.getExterior().getProperties().get(i).getValue());
            }
            expandableListDetail.put(specModel.getExterior().getName(), extriorTitle);
        }


        if (specModel.getInterior() != null) {
            for (int i = 0; i < specModel.getInterior().getProperties().size(); i++) {
                driveTrainTitle.add(specModel.getInterior().getProperties().get(i).getValue());
            }
            expandableListDetail.put(specModel.getInterior().getName(), interioTitle);

        }

        if (specModel.getSafty() != null) {
            for (int i = 0; i < specModel.getSafty().getProperties().size(); i++) {
                driveTrainTitle.add(specModel.getSafty().getProperties().get(i).getValue());
            }
            expandableListDetail.put(specModel.getSafty().getName(), safetyTitle);
        }


        return expandableListDetail;
    }

}
