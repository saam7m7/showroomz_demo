package com.mhamdi.showroomz.presentation

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.mhamdi.showroomz.R
import com.mhamdi.showroomz.data.Cache
import org.json.JSONException
import java.io.IOException

class SplashActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applyFullScreen()
        setContentView(R.layout.activity_splash)


        try {
            val thread = Thread(
                    Runnable {
                        Cache.INSTANCE.CheckIfWeHaveModels()
                        runOnUiThread { Handler().post { goToMainActivity() } }
                    }
            )
            thread.start()

        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        }


    }

    private fun applyFullScreen() {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val actionBar = supportActionBar
        actionBar?.hide()

    }

    private fun goToMainActivity() {
        if (!isFinishing) {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }
    }
}
