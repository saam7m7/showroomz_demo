package com.mhamdi.showroomz.presentation.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mhamdi.showroomz.data.models.SpecModel;
import com.mhamdi.showroomz.presentation.fragments.PagerFrag;

import java.util.List;

public class specModelsVIewPagerAdapter extends FragmentStatePagerAdapter {
    public static final String SPEC_MODEL = "SpecModel";
    private List<SpecModel> specModels;

    public specModelsVIewPagerAdapter(@NonNull FragmentManager fm, int behavior, List<SpecModel> specModels) {
        super(fm, behavior);
        this.specModels = specModels;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        PagerFrag pagerFrag = new PagerFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SPEC_MODEL, specModels.get(position));
        pagerFrag.setArguments(bundle);
        return pagerFrag;
    }

    @Override
    public int getCount() {
        return specModels.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return specModels.get(position).getName();
    }
}
