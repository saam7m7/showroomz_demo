package com.mhamdi.showroomz.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.mhamdi.showroomz.R;
import com.mhamdi.showroomz.data.Cache;
import com.mhamdi.showroomz.data.Exceptions.ItemNotFoundExcpetion;
import com.mhamdi.showroomz.data.models.Item;
import com.mhamdi.showroomz.data.models.SpecModel;
import com.mhamdi.showroomz.presentation.adapter.specModelsVIewPagerAdapter;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import static com.mhamdi.showroomz.presentation.adapter.ItemAdapter.MODEL_ID;

public class DetailActivity extends AppCompatActivity {

    public static final String INTERIOR_URL = "INTERIOR_URL";
    public static final String EXTERIOR_URL = "EXTERIOR_URL";
    private Item item;
    private List<SpecModel> specModels;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public DetailActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView image = findViewById(R.id.item_detail_image);
        TextView priceTxv = findViewById(R.id.price);
        TextView modelYear = findViewById(R.id.model_year_txv);
        tabLayout = findViewById(R.id.model_specs_tablayout);
        viewPager = findViewById(R.id.model_specs_view_pager);
        NestedScrollView scrollView = findViewById(R.id.nest_scrollview);
        ImageButton InteriorBtn = findViewById(R.id.interior);
        ImageButton ExteriorBtn = findViewById(R.id.exterior);
        scrollView.setFillViewport(true);

        InteriorBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, InteriorActivity.class);
            intent.putExtra(INTERIOR_URL, item.getModelsInteriorImage());
            this.startActivity(intent);
        });

        ExteriorBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, ExteriorActivity.class);
            intent.putExtra(EXTERIOR_URL, item.getModelsExteriorImage());
            this.startActivity(intent);
        });

        //get data
        Intent intent = getIntent();
        try {
            item = Cache.INSTANCE.getItemById(intent.getStringExtra(MODEL_ID));
            specModels = item.getSpecModels();
        } catch (ItemNotFoundExcpetion itemNotFoundExcpetion) {
            itemNotFoundExcpetion.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //present data
        toolbar.setTitle(item.getModelsNameEn());
        priceTxv.setText(item.getModelsPrice());
        modelYear.setText(item.getModelsYear());
        setUpPagerTabLayout();

        Glide.with(this).load(item.getModelsImage())
                .placeholder(R.drawable.vehicle_placeholder)
                .into(image);
    }


    private void setUpPagerTabLayout() {

        if (specModels != null) {
            specModelsVIewPagerAdapter adapter = new specModelsVIewPagerAdapter(getSupportFragmentManager(), 1, specModels);
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);

        }
    }
}
