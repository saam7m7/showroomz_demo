package com.mhamdi.showroomz.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mhamdi.showroomz.R;
import com.mhamdi.showroomz.data.Cache;
import com.mhamdi.showroomz.data.Exceptions.NoItemsException;
import com.mhamdi.showroomz.data.InDiskCache;
import com.mhamdi.showroomz.data.models.Item;
import com.mhamdi.showroomz.presentation.adapter.ItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Item> itemsList = new ArrayList<>();
    ItemAdapter adapter;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.items_list_recycler);


        try {
            itemsList = Cache.INSTANCE.getAllItems();
            adapter = new ItemAdapter(itemsList, this,this);
        } catch (NoItemsException e) {
            e.printStackTrace();
        } catch (Exception exp) {
            exp.printStackTrace();
        }

        recyclerView.setAdapter(adapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);

        ImageButton btnRefresh = findViewById(R.id.btn_refresh);
        btnRefresh.setOnClickListener(this::clearCache);

    }



    private void clearCache(View view){
        InDiskCache.INSTANCE.clearCache();
        Cache.INSTANCE.clearCache();
        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }


}
