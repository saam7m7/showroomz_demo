package com.mhamdi.showroomz.data.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Item implements Serializable {

    private String brandsHotline;
    private String modelsId;
    private String modelsType;
    private String modelsInformationEn;
    private String modelsInformationAr;
    private String modelsBrandId;
    private String modelsDescEn;
    private String modelsDescAr;
    private String modelsNameEn;
    private String modelsNameAr;
    private String modelsImage;
    private String modelsIsinitial;
    private String modelsYear;
    private String modelsColor;
    private String modelsView;
    private String modelsTestdriveAvailable;
    private String modelsTestdriveAtshowroom;
    private String modelsTestdriveBydelivery;
    private String modelsTestDrivePrice;
    private String modelsExteriorImage;
    private String modelsInteriorAvailable;
    private String modelsInteriorImage;
    private String modelsPrice;
    private String modelsComingsoonMonth;
    private String modelsSpecEn;
    private String modelsSpecAr;
    private List<SpecModel> specModels;

    public List<SpecModel> getSpecModels() {
        return specModels;
    }

    public void setSpecModels(List<SpecModel> specModels) {
        this.specModels = specModels;
    }

    public void addSpecModel(SpecModel specModel) {
        if (specModels == null) specModels = new ArrayList<>();
        specModels.add(specModel);
    }

    public String getBrandsHotline() {
        return brandsHotline;
    }

    public void setBrandsHotline(String brandsHotline) {
        this.brandsHotline = brandsHotline;
    }

    public String getModelsId() {
        return modelsId;
    }

    public void setModelsId(String models_id) {
        this.modelsId = models_id;
    }

    public String getModelsType() {
        return modelsType;
    }

    public void setModelsType(String models_type) {
        this.modelsType = models_type;
    }

    public String getModelsInformationEn() {
        return modelsInformationEn;
    }

    public void setModelsInformationEn(String models_information_en) {
        this.modelsInformationEn = models_information_en;
    }

    public String getModelsInformationAr() {
        return modelsInformationAr;
    }

    public void setModelsInformationAr(String models_information_ar) {
        this.modelsInformationAr = models_information_ar;
    }

    public String getModelsBrandId() {
        return modelsBrandId;
    }

    public void setModelsBrandId(String models_brand_id) {
        this.modelsBrandId = models_brand_id;
    }

    public String getModelsDescEn() {
        return modelsDescEn;
    }

    public void setModelsDescEn(String models_desc_en) {
        this.modelsDescEn = models_desc_en;
    }

    public String getModelsDescAr() {
        return modelsDescAr;
    }

    public void setModelsDescAr(String models_desc_ar) {
        this.modelsDescAr = models_desc_ar;
    }

    public String getModelsNameEn() {
        return modelsNameEn;
    }

    public void setModelsNameEn(String modelsNameEn) {
        this.modelsNameEn = modelsNameEn;
    }

    public String getModelsNameAr() {
        return modelsNameAr;
    }

    public void setModelsNameAr(String modelsNameAr) {
        this.modelsNameAr = modelsNameAr;
    }

    public String getModelsImage() {
        return modelsImage;
    }

    public void setModelsImage(String modelsImage) {
        this.modelsImage = modelsImage;
    }

    public String getModelsIsinitial() {
        return modelsIsinitial;
    }

    public void setModelsIsinitial(String modelsIsinitial) {
        this.modelsIsinitial = modelsIsinitial;
    }

    public String getModelsYear() {
        return modelsYear;
    }

    public void setModelsYear(String modelsYear) {
        this.modelsYear = modelsYear;
    }

    public String getModelsColor() {
        return modelsColor;
    }

    public void setModelsColor(String modelsColor) {
        this.modelsColor = modelsColor;
    }

    public String getModelsView() {
        return modelsView;
    }

    public void setModelsView(String modelsView) {
        this.modelsView = modelsView;
    }

    public String getModelsTestdriveAvailable() {
        return modelsTestdriveAvailable;
    }

    public void setModelsTestdriveAvailable(String modelsTestdriveAvailable) {
        this.modelsTestdriveAvailable = modelsTestdriveAvailable;
    }

    public String getModelsTestdriveAtshowroom() {
        return modelsTestdriveAtshowroom;
    }

    public void setModelsTestdriveAtshowroom(String modelsTestdriveAtshowroom) {
        this.modelsTestdriveAtshowroom = modelsTestdriveAtshowroom;
    }

    public String getModelsTestdriveBydelivery() {
        return modelsTestdriveBydelivery;
    }

    public void setModelsTestdriveBydelivery(String modelsTestdriveBydelivery) {
        this.modelsTestdriveBydelivery = modelsTestdriveBydelivery;
    }

    public String getModelsTestDrivePrice() {
        return modelsTestDrivePrice;
    }

    public void setModelsTestDrivePrice(String modelsTestDrivePrice) {
        this.modelsTestDrivePrice = modelsTestDrivePrice;
    }

    public String getModelsExteriorImage() {
        return modelsExteriorImage;
    }

    public void setModelsExteriorImage(String modelsExteriorImage) {
        this.modelsExteriorImage = modelsExteriorImage;
    }

    public String getModelsInteriorAvailable() {
        return modelsInteriorAvailable;
    }

    public void setModelsInteriorAvailable(String modelsInteriorAvailable) {
        this.modelsInteriorAvailable = modelsInteriorAvailable;
    }

    public String getModelsInteriorImage() {
        return modelsInteriorImage;
    }

    public void setModelsInteriorImage(String modelsInteriorImage) {
        this.modelsInteriorImage = modelsInteriorImage;
    }

    public String getModelsPrice() {
        return modelsPrice;
    }

    public void setModelsPrice(String modelsPrice) {
        this.modelsPrice = modelsPrice;
    }

    public String getModelsComingsoonMonth() {
        return modelsComingsoonMonth;
    }

    public void setModelsComingsoonMonth(String modelsComingsoonMonth) {
        this.modelsComingsoonMonth = modelsComingsoonMonth;
    }

    public String getModelsSpecEn() {
        return modelsSpecEn;
    }

    public void setModelsSpecEn(String modelsSpecEn) {
        this.modelsSpecEn = modelsSpecEn;
    }

    public String getModelsSpecAr() {
        return modelsSpecAr;
    }

    public void setModelsSpecAr(String modelsSpecAr) {
        this.modelsSpecAr = modelsSpecAr;
    }
}

