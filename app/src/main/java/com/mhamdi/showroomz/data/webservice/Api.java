package com.mhamdi.showroomz.data.webservice;

import android.util.Log;

import com.mhamdi.showroomz.BuildConfig;
import com.mhamdi.showroomz.data.InDiskCache;
import com.mhamdi.showroomz.data.models.Item;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public enum Api {
    INSTANCE;

    public static final String DATA = "data";
    public static final String URL = "http://showroomz.com/api/api.php?function=allmodels";
    public static final String HOST = "Host";
    public static final String HOST_NAME = "showroomz.com";
    public static final String ACCEPT_ENCODING = "accept-encoding";
    public static final String GZIP_DEFLATE = "gzip, deflate";
    public static final String CONNECTION = "Connection";
    public static final String KEEP_ALIVE = "keep-alive";
    public static final String CACHE_CONTROL = "cache-control";
    public static final String NO_CACHE = "no-cache";


    public Map<String, Item> getAllItems() throws IOException, JSONException {


        String response;

        if(InDiskCache.INSTANCE.cacheExist()){
            response = InDiskCache.INSTANCE.getCache();
        }else {
            response = makeWebRequest();
            InDiskCache.INSTANCE.setCache(response);
        }

        if(response == null) throw  new IOException();

        Map<String, Item> itemsMap = new HashMap<>();
            JSONObject json = new JSONObject(response);
            JSONArray jsonArray = json.getJSONArray(DATA);


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                Item item = ItemParser.Parse(jsonItem);

                itemsMap.put(item.getModelsId(), item);

                if (BuildConfig.DEBUG)
                    Log.d("Log", "Name: " + item.getModelsNameEn());
            }
        return itemsMap;
    }

    @NotNull
    private String makeWebRequest() throws IOException {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(0, TimeUnit.MILLISECONDS)
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .writeTimeout(0, TimeUnit.MILLISECONDS).build();


        Request request = new Request.Builder()
                .url(URL)
                .get()
                .addHeader(HOST, HOST_NAME)
                .addHeader(ACCEPT_ENCODING, GZIP_DEFLATE)
                .addHeader(CONNECTION, KEEP_ALIVE)
                .addHeader(CACHE_CONTROL, NO_CACHE)
                .build();


        Response response = client.newCall(request).execute();
        if(!response.isSuccessful()) throw new IOException();
        return response.body().string();
    }
}
