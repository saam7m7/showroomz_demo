package com.mhamdi.showroomz.data;

import com.mhamdi.showroomz.data.Exceptions.ItemNotFoundExcpetion;
import com.mhamdi.showroomz.data.Exceptions.NoItemsException;
import com.mhamdi.showroomz.data.models.Item;
import com.mhamdi.showroomz.data.webservice.Api;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Cache {

    INSTANCE;

    private Map<String, Item> allItems = new HashMap<>();
    private List<Item> allItemsList = new ArrayList<>();

    public List<Item> getAllItems() throws NoItemsException, NullPointerException, IOException, JSONException {

        CheckIfWeHaveModels();

        return allItemsList;

    }

    public void CheckIfWeHaveModels() throws IOException, JSONException {
        if (allItems.size() == 0) {
            allItems = Api.INSTANCE.getAllItems();
            for (Map.Entry<String, Item> itemEntry : allItems.entrySet()) {
                allItemsList.add((Item) ((Map.Entry) itemEntry).getValue());
            }
        }
    }

    public Item getItemById(String id) throws ItemNotFoundExcpetion, NullPointerException, IOException, JSONException {
        CheckIfWeHaveModels();
        if (!allItems.containsKey(id)) throw new ItemNotFoundExcpetion();
        return allItems.get(id);
    }

    public void clearCache(){
        allItems = new HashMap<>();
        allItemsList = new ArrayList<>();
    }

}
