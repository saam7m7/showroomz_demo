package com.mhamdi.showroomz.data.webservice;

import com.mhamdi.showroomz.data.models.SpecModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


class ModelParser {
    private static final String EXTERIOR_FEATURES = "EXTERIOR FEATURES";
    private static final String INTERIOR_FEATURES = "INTERIOR FEATURES";
    private static final String DRIVE_TRAIN = "DRIVETRAIN";
    private static final String SAFETY_SECURITY = "SAFETY & SECURITY";

    static SpecModel Parse(String key, JSONArray array) throws JSONException {
        SpecModel specModel = new SpecModel();
        specModel.setName(key);
        if (array == null || array.length() == 0) return specModel;

        for (int i = 0; i < array.length(); i++) {
            JSONObject features = array.getJSONObject(i);
            if (features.has(DRIVE_TRAIN))
                specModel.setDriveTrain(FeatureParser.Parse(DRIVE_TRAIN, features));
            if (features.has(EXTERIOR_FEATURES))
                specModel.setExterior(FeatureParser.Parse(EXTERIOR_FEATURES, features));
            if (features.has(INTERIOR_FEATURES))
                specModel.setInterior(FeatureParser.Parse(INTERIOR_FEATURES, features));
            if (features.has(SAFETY_SECURITY))
                specModel.setSafty(FeatureParser.Parse(SAFETY_SECURITY, features));
        }

        return specModel;
    }
}
