package com.mhamdi.showroomz.data;

import io.paperdb.Paper;

public enum InDiskCache {

    INSTANCE;

    public static final String Key = "data";

    public boolean cacheExist() {
        return Paper.book().contains(Key);
    }

    public String getCache() {
        return Paper.book().read(Key);
    }

    public void setCache(String jsonData) {
        Paper.book().write(Key, jsonData);
    }

    public void clearCache(){
        Paper.book().delete(Key);
    }

}
