package com.mhamdi.showroomz.data.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Feature implements Serializable {

    protected String name;
    private List<LabelValuePair> properties;

    public Feature(String name) {
        this.name = name;
    }

    public Feature(List<LabelValuePair> properties) {
        this.properties = properties;
    }

    public List<LabelValuePair> getProperties() {
        return properties;
    }

    public void setProperties(List<LabelValuePair> properties) {
        this.properties = properties;
    }

    public void AddProperty(LabelValuePair property) {
        if (properties == null) properties = new ArrayList<>();
        properties.add(property);
    }

    public String getName() {
        return name;
    }
}
