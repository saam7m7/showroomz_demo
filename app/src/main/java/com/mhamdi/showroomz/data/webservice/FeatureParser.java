package com.mhamdi.showroomz.data.webservice;

import com.mhamdi.showroomz.data.models.Feature;
import com.mhamdi.showroomz.data.models.LabelValuePair;
import com.mhamdi.showroomz.helper.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class FeatureParser {

    private static final String LABEL = "label";
    private static final String VALUE = "value";

    static Feature Parse(String key, JSONObject jsonObject) throws JSONException {

        JSONArray features = jsonObject.getJSONArray(key);
        Feature feature = new Feature(key);

        for (int i = 0; i < features.length(); i++) {
            if (!Utils.isJsonValid(features.getString(i))) return feature;
            JSONObject jsonFeature = new JSONObject(features.getString(i));
            LabelValuePair pair = new LabelValuePair(jsonFeature.getString(LABEL), jsonFeature.getString(VALUE));
            feature.AddProperty(pair);
        }

        return feature;


    }


}

