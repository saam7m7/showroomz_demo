package com.mhamdi.showroomz.data.models;

import java.io.Serializable;

public class SpecModel implements Serializable {

    private String name;
    private Feature driveTrain;
    private Feature safty;
    private Feature exterior;
    private Feature interior;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Feature getDriveTrain() {
        return driveTrain;
    }

    public void setDriveTrain(Feature driveTrain) {
        this.driveTrain = driveTrain;
    }

    public Feature getSafty() {
        return safty;
    }

    public void setSafty(Feature safty) {
        this.safty = safty;
    }

    public Feature getExterior() {
        return exterior;
    }

    public void setExterior(Feature exterior) {
        this.exterior = exterior;
    }

    public Feature getInterior() {
        return interior;
    }

    public void setInterior(Feature interior) {
        this.interior = interior;
    }
}
