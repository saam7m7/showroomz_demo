package com.mhamdi.showroomz.data.webservice;

import android.util.Log;

import com.mhamdi.showroomz.BuildConfig;
import com.mhamdi.showroomz.data.models.Item;
import com.mhamdi.showroomz.data.models.SpecModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

class ItemParser {

    private static final String UPLOAD_URL = "http://showroomz.com/api/";
    private static final String BRANDS_HOTLINE = "brands_hotline";
    private static final String MODELS_ID = "models_id";
    private static final String MODELS_TYPE = "models_type";
    private static final String MODELS_INFORMATION_EN = "models_information_en";
    private static final String MODELS_INFORMATION_AR = "models_information_ar";
    private static final String MODELS_BRAND_ID = "models_brand_id";
    private static final String MODELS_DESC_EN = "models_desc_en";
    private static final String MODELS_DESC_AR = "models_desc_ar";
    private static final String MODELS_NAME_EN = "models_name_en";
    private static final String MODELS_NAME_AR = "models_name_ar";
    private static final String MODELS_IMAGE = "models_image";
    private static final String MODELS_ISINITIAL = "models_isinitial";
    private static final String MODELS_YEAR = "models_year";
    private static final String MODELS_COLOR = "models_color";
    private static final String MODELS_VIEW = "models_view";
    private static final String MODELS_TESTDRIVE_AVAILABLE = "models_testdrive_available";
    private static final String MODELS_TESTDRIVE_ATSHOWROOM = "models_testdrive_atshowroom";
    private static final String MODELS_TESTDRIVE_BYDELIVERY = "models_testdrive_bydelivery";
    private static final String MODELS_TEST_DRIVE_PRICE = "models_test_drive_price";
    private static final String MODELS_EXTERIOR_IMAGE = "models_exterior_image";
    private static final String MODELS_INTERIOR_AVAILABLE = "models_interior_available";
    private static final String MODELS_INTERIOR_IMAGE = "models_interior_image";
    private static final String MODELS_PRICE = "models_price";
    private static final String MODELS_COMINGSOON_MONTH = "models_comingsoon_month";
    private static final String MODELS_SPEC_AR = "models_spec_ar";
    private static final String MODELS_SPEC_EN = "models_spec_en";

    static Item Parse(JSONObject jsonItem) throws JSONException {
        Item item = new Item();

        item.setBrandsHotline(jsonItem.getString(BRANDS_HOTLINE));
        item.setModelsId(jsonItem.getString(MODELS_ID));
        item.setModelsType(jsonItem.getString(MODELS_TYPE));
        item.setModelsInformationEn(jsonItem.getString(MODELS_INFORMATION_EN));
        item.setModelsInformationAr(jsonItem.getString(MODELS_INFORMATION_AR));
        item.setModelsBrandId(jsonItem.getString(MODELS_BRAND_ID));
        item.setModelsDescEn(jsonItem.getString(MODELS_DESC_EN));
        item.setModelsDescAr(jsonItem.getString(MODELS_DESC_AR));
        item.setModelsNameEn(jsonItem.getString(MODELS_NAME_EN));
        item.setModelsNameAr(jsonItem.getString(MODELS_NAME_AR));
        item.setModelsImage(UPLOAD_URL + jsonItem.getString(MODELS_IMAGE));
        item.setModelsIsinitial(jsonItem.getString(MODELS_ISINITIAL));
        item.setModelsYear(jsonItem.getString(MODELS_YEAR));
        item.setModelsColor(jsonItem.getString(MODELS_COLOR));
        item.setModelsView(jsonItem.getString(MODELS_VIEW));
        item.setModelsTestdriveAvailable(jsonItem.getString(MODELS_TESTDRIVE_AVAILABLE));
        item.setModelsTestdriveAtshowroom(jsonItem.getString(MODELS_TESTDRIVE_ATSHOWROOM));
        item.setModelsTestdriveBydelivery(jsonItem.getString(MODELS_TESTDRIVE_BYDELIVERY));
        item.setModelsTestDrivePrice(jsonItem.getString(MODELS_TEST_DRIVE_PRICE));
        item.setModelsExteriorImage(jsonItem.getString(MODELS_EXTERIOR_IMAGE));
        item.setModelsInteriorAvailable(jsonItem.getString(MODELS_INTERIOR_AVAILABLE));
        item.setModelsInteriorImage(jsonItem.getString(MODELS_INTERIOR_IMAGE));
        item.setModelsPrice(jsonItem.getString(MODELS_PRICE));
        item.setModelsComingsoonMonth(jsonItem.getString(MODELS_COMINGSOON_MONTH));
        item.setModelsSpecAr(jsonItem.getString(MODELS_SPEC_AR));
        item.setModelsSpecEn(jsonItem.getString(MODELS_SPEC_EN));
        String spec = jsonItem.getString(MODELS_SPEC_EN);


        JSONObject specJson = new JSONObject(spec);

        Iterator<String> keys = specJson.keys();

        while (keys.hasNext()) {
            String key = keys.next();
            if (BuildConfig.DEBUG)
                Log.d("TAG", "keys: " + key);
            SpecModel specModel = ModelParser.Parse(key, specJson.getJSONArray(key));


            item.addSpecModel(specModel);

        }

        if (BuildConfig.DEBUG)
            Log.d("TAG", "spec models size: " + item.getSpecModels().size());
        return item;

    }
}
