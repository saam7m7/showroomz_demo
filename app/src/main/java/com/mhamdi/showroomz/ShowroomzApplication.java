package com.mhamdi.showroomz;

import android.app.Application;

import io.paperdb.Paper;

public class ShowroomzApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(this);
    }
}
